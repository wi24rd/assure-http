# A demo program for ASSURE http version
Please use `mvn clean spring-boot:run` to run.

# Notice
This is NOT a product. 
There is NO WARRANTY, to the extent permitted by law.
Written by @wi24rd for testing, educational and fun purposes.
