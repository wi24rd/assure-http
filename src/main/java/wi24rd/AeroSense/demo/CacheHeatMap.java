package wi24rd.AeroSense.demo;

import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
@Component
public class CacheHeatMap {

    private final ConcurrentHashMap<String, byte[]> cache = new ConcurrentHashMap<>();

    /**
     *  cache heatMap to one file
     * @param radar radar Uuid
     * @param tag 0:over 1:not over
     * @param data heatMap data
     * @return more frame to one file
     */
    public byte[] cacheHeatMap(String radar, int tag, byte[] data) {
        byte[] bytes1 = cache.get(radar);
        byte[] bytes2;

        if (bytes1 == null) {
            if (tag == 0) {
                return data;
            }
            //unfinished, get cache
            cache.put(radar, data);
            return null;
        } else {
            bytes2 = new byte[bytes1.length + data.length];
            System.arraycopy(bytes1, 0, bytes2, 0, bytes1.length);
            System.arraycopy(data, 0, bytes2, bytes1.length, data.length);
            cache.put(radar, bytes2);
            if (tag == 0) {
                cache.remove(radar);
                return bytes2;
            }
        }
        return null;
    }
}